// Full Screeen Slider Code

var $item = $('.carousel .item');
var $wHeight = $(window).height();
$item.eq(0).addClass('active');
$item.height($wHeight);
$item.addClass('full-screen');

$('.carousel img').each(function() {
  var $src = $(this).attr('src');
  var $color = $(this).attr('data-color');
  $(this).parent().css({
    'background-image' : 'url(' + $src + ')',
    'background-color' : $color
  });
  $(this).remove();
});

$(window).on('resize', function (){
  $wHeight = $(window).height();
  $item.height($wHeight);
});

$('.carousel').carousel({
  interval: 6000,
  pause: "false"
});

// Custom Jquery Carousel image on slide change
// http://stackoverflow.com/questions/40344822/how-to-cycle-a-class-through-siblings-on-an-event
$('#myCarousel').on('slid.bs.carousel', function () {
  var i = $('.item.active').index();
  $('.caption').removeClass('visible').eq(i).addClass('visible');
});

$('.footer-carousel').carousel({
  interval: 6000,
  pause: "false"
});

// Fiting video to whole width script https://github.com/davatron5000/FitVids.js
$(".vid-ban").fitVids();

// Initiate animate on scroll library
AOS.init();

$('.hamburger1').click(function () {
  $('.hamburger1').toggleClass("is-active");
  $('#navigation').slideToggle();
  $('#navigation a:link, #navigation a:visited').click(function() {
    $('#navigation').hide();
    $('.hamburger1').removeClass("is-active");
  });
});

$('.hamburger2').click(function () {
  $('.hamburger2').toggleClass("is-active");
  $('#navigation2').slideToggle();
  $('#navigation2 a:link, #navigation2 a:visited').click(function() {
    $('#navigation2').hide();
    $('.hamburger2').removeClass("is-active");
  });
});


// Scroll Navigation
// Customized from http://stackoverflow.com/questions/15798360/show-div-on-scrolldown-after-800px
$(document).scroll(function() {
  var y = $(this).scrollTop();
  if (y > 400) {
    $('.scroll-navigation').fadeIn("slow");
  } else {
    $('.scroll-navigation').fadeOut();
  }
});
